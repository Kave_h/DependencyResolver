﻿using System.Collections.Generic;
using System.Linq;
using DC.Domain;
using DC.Domain.Command;
using DC.Domain.Model;

namespace DC.Infrastructure
{
    public class DependencyResolverService : IDependencyResolverService
    {
        public DependecyNode CreateDependecyTree(string[] items)
        {
            var emptyPackage = Package.Empty();
            var root = DependecyNode.CreateNode(emptyPackage);
            var tree = root;

            if (items.Length <= PackageHelper.PackageNumber(items) + 1) return root;

            var packageDependencies = PackageHelper.GetPackageDependencies(items);

            foreach (var packageDependency in packageDependencies)
            {
                var key = packageDependency[0];
                var dt = DependecyNode.FindNode(root, key);
                tree = dt ?? root.AddChild(DependecyNode.CreateNode(key));
                for (var i = 1; i < packageDependency.Length; i++)
                {
                    //does not add to tree if package depends on its self
                    if (key.Equals(packageDependency[i])) continue;

                    var node = DependecyNode.FindNode(root, packageDependency[i]);

                    if (node != null) tree.AddChild(node);//probably it causes circular dependency 
                    else
                    {
                        tree.AddChild(packageDependency[i]);
                    }
                }
            }
            return root;
        }

        public (bool isValid, ResolveErrorType validationErrorType) ValidateTree(IEnumerable<DependecyNode> nodes)
        {
            var buff = new List<Package>();
            foreach (var node in nodes)
            {
                var result =  TraverseTree(node, ref buff);
                if (result.isValid || (result.validationErrorType == ResolveErrorType.CircularDependency &&
                                       DependecyNode.IgnoreCircularDependecy))
                    continue;
                return result;
            }
            return (true, ResolveErrorType.None);
        }

        public (bool isValid, ResolveErrorType validationErrorType) ValidateTree(DependecyNode node)
        {
            return ValidateTree(new[] {node});
        }

        public bool ValidateMainPackages(string[] items)
        {
            if (PackageHelper.GetMainPackages(items).Count() == PackageHelper.GetMainPackages(items).Select(i => i.Name).Distinct().Count())
                return true;
            return false;
        }

        private (bool isValid, ResolveErrorType validationErrorType) TraverseTree(DependecyNode root,ref List<Package> buffer)
        {
            (bool valid, ResolveErrorType validationErrorType) output = (true, ResolveErrorType.None);
            if (buffer.Exists(i => i.Equals(root.Package.Name) && !i.Equals(root.Package)))
            {
                //more than one package version needs to install 
                output.validationErrorType = ResolveErrorType.MultipleVersionsSamePackage;
                output.valid = false;
            }
            else if (buffer.Exists(i => i.Equals(root.Package)))
            {
                //circular dependecy found 
                output.validationErrorType = ResolveErrorType.CircularDependency;
                output.valid = false;
            }
            else
            {
                buffer.Add(root.Package);
                foreach (var item in root.DependOnPackages)
                {
                    output =  TraverseTree(item, ref buffer);
                }
            }
            return output;
        }
    }
}