﻿using System.Collections.Generic;
using System.Linq;
using DC.Domain.Model;

namespace DC.Infrastructure
{
    public static class PackageHelper
    {
        public static List<Package> GetMainPackages(string[] items)
        {
            var packagesNumber = int.Parse(items[0]);
            return items.Skip(1).Take(packagesNumber)
                .Select(item => GetPackageVersion(item).FirstOrDefault()).ToList();
        }

        public static int PackageNumber(string[] items) => int.Parse(items[0]);

        public static IEnumerable<Package> GetPackageVersion(string str)
        {
            var split = str.Split(',');
            for (var i = 0; i < split.Length; i += 2)
            {
                yield return Package.New(split[i], split[i + 1]);
            }
        }

        public static List<Package[]> GetPackageDependencies(string[] items)
        {
            var packageDependenciesNumber = int.Parse(items[PackageNumber(items) + 1]);
            var packageDependencies = items.Skip(PackageNumber(items) + 2).Take(packageDependenciesNumber)
                .Select(item => GetPackageVersion(item).ToArray()).ToList();
            return packageDependencies;
        }
    }
}