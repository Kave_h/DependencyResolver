﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DC.Domain;
using DC.Domain.Command;
using DC.Domain.Model;
using DC.Domain.Seedwork;

namespace DC.Infrastructure.CommandExecuter
{
    public class ResolveDependeciesCommandExecuter : ICommandExecuter<ResolveDependeciesCommand,bool>
    {
        private readonly IDependencyResolverService _resolverService;

        public ResolveDependeciesCommandExecuter(IDependencyResolverService resolverService)
        {
            _resolverService = resolverService;
        }

        public bool Execute(ResolveDependeciesCommand command)
        {

            var tree = _resolverService.CreateDependecyTree(command.Items);

            //check if there are packages with different version in the install list 
            if (!_resolverService.ValidateMainPackages(command.Items)) return false;

            if (tree.IsEmpty()) return true;
            var packages = PackageHelper.GetMainPackages(command.Items);
            var result = _resolverService.ValidateTree(packages.Select(i => DependecyNode.FindNode(tree, i)));
            return result.isValid;
        }

        public async Task<bool> ExecuteAsync(ResolveDependeciesCommand command)
        {
            throw new NotImplementedException();

        }
    }
}