﻿using System.Threading.Tasks;
using DC.Domain.Command;
using DC.Domain.Seedwork;

namespace DC.Infrastructure.CommandExecuter
{
    public sealed class ResolveDependeciesCommandValidator : ICommandValidator<ResolveDependeciesCommand>
    {
        public bool Validate(ResolveDependeciesCommand command)
        {
            if (!int.TryParse(command.Items[0], out var numberOfPackages)) return false;
            if (command.Items.Length < numberOfPackages + 1) return false;
            for (int i = 0; i < numberOfPackages; i++)
            {
                if (command.Items[i + 1].Split(',').Length != 2) return false;
            }
            if (command.Items.Length == numberOfPackages + 1) return true;

            if (!int.TryParse(command.Items[numberOfPackages + 1], out var numberOfDependecies)) return false;

            if (command.Items.Length < numberOfPackages + numberOfDependecies + 1) return false;

            for (int i = 0; i < numberOfDependecies; i++)
            {
                if (command.Items[i + numberOfPackages + 2].Split(',').Length % 2 == 1) return false;
            }
            return true;
        }

        public async Task<bool> ValidateAsync(ResolveDependeciesCommand command)
        {
            throw new System.NotImplementedException();
        }
    }
}