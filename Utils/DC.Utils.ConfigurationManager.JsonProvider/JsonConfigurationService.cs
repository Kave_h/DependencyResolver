﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using DC.Utils.Interface;

namespace DC.Utils.ConfigurationManager.JsonProvider
{
    public sealed class JsonConfigurationService<T> : IConfigurationService<T> where T : BaseConfigurationModel
    {
        private static string FileName => "Config.json";
        private static string DebugFileName => "Config-debug.json";

        private readonly ISerializer _serializer;
        private readonly ICacher _memoryCache;
        private readonly int _configCacheIntervalInSeconds;
        private const string CacheKey = "configKey";

        public JsonConfigurationService(ISerializer serializer, ICacher memoryCache)
        {
            _serializer = serializer;
            _memoryCache = memoryCache;
            _configCacheIntervalInSeconds = 300;
        }

        public T Get()
        {
            var key = string.Intern($"{CacheKey}.{typeof(T).Name}");
            var value = _memoryCache.GetValue(key);
            if (value != null)
                return value as T;
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"AppData", FileName);
            GetDebugConfigFileName(ref filePath);
            var fileContent = File.ReadAllText(filePath);
            var configModel = _serializer.Deserialize<T>(fileContent);
            _memoryCache.SetValue(key, configModel,
                new CacheConfiguration
                {
                    SlidingExpiration = TimeSpan.FromSeconds(_configCacheIntervalInSeconds)
                });
            return configModel;
        }

        public async Task<T> GetAsync()
        {
            throw new NotImplementedException();
        }

        [Conditional("DEBUG")]
        private void GetDebugConfigFileName(ref string path)
        {

            var debugfilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"AppData", DebugFileName);
            if (File.Exists(debugfilePath))
                path = debugfilePath;
        }
    }
}
