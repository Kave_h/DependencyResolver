﻿using System;

namespace DC.Utils.Interface
{
    public interface ILogger
    {
        void LogInformation(string sender, string message, bool urgent = false);
        void LogWarning(string sender, string message);
        void LogError(string sender, string message, Exception ex);
    }
}