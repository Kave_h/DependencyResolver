﻿using System;

namespace DC.Utils.Interface
{
    public interface ISerializer
    {
        string Serialize<T>(T data, SerializerConfig configuration = null);
        T Deserialize<T>(string json, SerializerConfig configuration = null);
        object DeserializeObject(string json, SerializerConfig configuration = null);
        dynamic DynamicDeserialize(string json);
    }

    public class SerializerConfig
    {
        public bool IgnoreNullValues { get; set; }
        public bool IgnoreDefaultValues { get; set; }
        public bool IncludeInherited { get; set; } = true;
        public string SerializeFormat { get; set; }
    }
}
