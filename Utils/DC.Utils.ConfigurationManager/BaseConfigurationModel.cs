﻿namespace DC.Utils.ConfigurationManager
{
    public abstract class BaseConfigurationModel
    {
        public string Version { get; set; }
    }
}