﻿using System;
using System.Threading.Tasks;

namespace DC.Utils.ConfigurationManager
{
    public interface IConfigurationService<T> where T : BaseConfigurationModel
    {
        T Get();
        Task<T> GetAsync();
    }
}
