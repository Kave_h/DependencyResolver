﻿using System;
using DC.Utils.Interface;
using Newtonsoft.Json;

namespace DC.Utils.NewtonSoftSerializer
{
    public sealed class NewtonSoftSerializer : ISerializer
    {
        private readonly IObjectMapper _mapper;

        public NewtonSoftSerializer(IObjectMapper mapper)
        {
            _mapper = mapper;
        }

        public string Serialize<T>(T data, SerializerConfig configuration = null)
        {
            var jsonConfig = _mapper.Map<JsonSerializerSettings>(configuration);
            return JsonConvert.SerializeObject(data, jsonConfig);
        }

        public T Deserialize<T>(string json, SerializerConfig configuration = null)
        {
            var jsonConfig = _mapper.Map<JsonSerializerSettings>(configuration);
            return JsonConvert.DeserializeObject<T>(json, jsonConfig);
        }

        public object DeserializeObject(string json, SerializerConfig configuration = null)
        {
            var jsonConfig = _mapper.Map<JsonSerializerSettings>(configuration);
            return JsonConvert.DeserializeObject(json, jsonConfig);
        }

        public dynamic DynamicDeserialize(string json)
        {
            throw new NotImplementedException();
        }
    }
}
