﻿using System;
using System.Diagnostics;
using DC.Utils.Interface;

namespace DC.Utils.DefaultLogger
{
    public class DCLogger : ILogger
    {
        public void LogInformation(string sender, string message, bool urgent = false)
        {
            if (Environment.UserInteractive)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine($"{message}");
            }
            Debugger.Log(0, sender, message);
        }

        public void LogWarning(string sender, string message)
        {
            if (Environment.UserInteractive)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"{message}");
            }
            Debugger.Log(1, sender, message);
        }

        public void LogError(string sender, string message, Exception ex)
        {
            if (Environment.UserInteractive)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{message}");
            }
            Debugger.Log(2, sender, $"{message} : {ex}");
        }
    }
}
