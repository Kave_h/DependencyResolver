﻿using System.Collections.Generic;
using DC.Domain.Model;

namespace DC.Domain
{
    public interface IDependencyResolverService
    {
        /// <summary>
        /// creates a tree out of dependencies and returns root node
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        DependecyNode CreateDependecyTree(string[] input);


        /// <summary>
        /// validate dependency tree against given nodes
        /// </summary>
        /// <param name="nodes"></param>
        /// <returns></returns>
        (bool isValid, ResolveErrorType validationErrorType) ValidateTree(IEnumerable<DependecyNode> nodes);

        /// <summary>
        /// validate dependency tree against given node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        (bool isValid, ResolveErrorType validationErrorType) ValidateTree(DependecyNode node);


        /// <summary>
        /// check if there are not duplicate package to install
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        bool ValidateMainPackages(string[] items);
    }
}