﻿using DC.Domain.Seedwork;

namespace DC.Domain.Command
{
    
    public sealed class ResolveDependeciesCommand : ICommand
    {
        public ResolveDependeciesCommand(string[] items)
        {
            Items = items;
        }

        public string[] Items { get; }
    }
}