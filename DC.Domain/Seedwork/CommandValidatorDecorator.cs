﻿using System;
using System.Threading.Tasks;

namespace DC.Domain.Seedwork
{
    public sealed class CommandValidatorDecorator<T,TResult> : ICommandExecuter<T,TResult> where T:ICommand
    {
        private readonly ICommandValidator<T> _validator;
        private readonly ICommandExecuter<T,TResult> _decoratee;

        public CommandValidatorDecorator(ICommandValidator<T> validator, ICommandExecuter<T,TResult> decoratee)
        {
            _validator = validator;
            _decoratee = decoratee;
        }

        public TResult Execute(T command)
        {
            if (!_validator.Validate(command))
                throw new Exception("dependency script is not valid");
            return _decoratee.Execute(command);
        }

        public async Task<TResult> ExecuteAsync(T command)
        {
            if(await _validator.ValidateAsync(command))
                throw new Exception("dependency script is not valid");
            return await _decoratee.ExecuteAsync(command);
        }
    }
}