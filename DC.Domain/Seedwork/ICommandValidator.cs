﻿using System.Threading.Tasks;

namespace DC.Domain.Seedwork
{
    public interface ICommandValidator<in T> where T : ICommand
    {
        bool Validate(T command);
        Task<bool> ValidateAsync(T command);
    }
}