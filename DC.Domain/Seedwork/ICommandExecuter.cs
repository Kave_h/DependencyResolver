﻿using System.Threading.Tasks;

namespace DC.Domain.Seedwork
{
    public interface ICommandExecuter<in T,TResult> where T : ICommand
    {
        TResult Execute(T command);
        Task<TResult> ExecuteAsync(T command);
    }
}