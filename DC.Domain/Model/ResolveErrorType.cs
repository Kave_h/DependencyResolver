﻿namespace DC.Domain.Model
{
    public enum ResolveErrorType
    {
        None = 0,
        MultipleVersionsSamePackage =1,
        CircularDependency = 2
    }
}