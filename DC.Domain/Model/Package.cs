﻿using System;

namespace DC.Domain.Model
{
    public class Package
    {
        public string Name { get; private set; }
        public string Version { get; private set; }

        public static Package New(string name, string version)
        {
            return new Package()
            {
                Name = name,
                Version = version
            };
        }

        public static Package Empty()
        {
            return new Package();
        }

        public bool IsEmpty()
        {
            if (string.IsNullOrEmpty(Name)) return true;
            return false;
        }

        public bool Equals(Package p)
        {
            if (p.IsEmpty() && IsEmpty())
                return true;
            if (p.IsEmpty() && !IsEmpty())
                return false;
            if (!p.IsEmpty() && IsEmpty())
                return false;
            if (Name.Equals(p.Name, StringComparison.CurrentCultureIgnoreCase) &&
                Version.Equals(p.Version, StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }
            return false;
        }

        public bool Equals(string packageName)
        {
            return Name != null && Name.Equals(packageName, StringComparison.CurrentCultureIgnoreCase);
        }

        public override string ToString()
        {
            return $"{Name}:{Version}";
        }
    }
}