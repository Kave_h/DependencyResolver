﻿using System.Collections.Generic;
using System.Linq;

namespace DC.Domain.Model
{
    public class DependecyNode
    {
        public static bool IgnoreCircularDependecy = true;
        public Package Package { get; internal set; }
        public List<DependecyNode> DependOnPackages { get; set; }
        public static DependecyNode CreateNode(Package p)
        {
            return new DependecyNode() { Package = p, DependOnPackages = new List<DependecyNode>() };
        }
        public static DependecyNode FindNode(DependecyNode root, Package package)
        {
            DependecyNode node = null;
            if (root.Package.Equals(package))
            {
                node = root;
            }
            else
            {
                foreach (var item in root.DependOnPackages)
                {
                    node = FindNode(item, package);
                    if (node != null) return node;
                }
            }
            return node;
        }
        public DependecyNode AddChild(DependecyNode node)
        {
            DependOnPackages.Add(node);
            return node;
        }
        public DependecyNode AddChild(Package node)
        {
            var p = DependecyNode.CreateNode(node);
            DependOnPackages.Add(p);
            return p;
        }
        public bool IsEmpty()
        {
            if (Package.IsEmpty() && !DependOnPackages.Any()) return true;
            return false;
        }
    }
}