﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DC.Bootstrapper;
using DC.Domain;
using DC.Domain.Model;
using DC.Utils.ConfigurationManager;
using DC.Utils.ConfigurationManager.JsonProvider;
using DC.Utils.Interface;
using DependencyChecker.Model;
using DependencyChecker.Provider;
using NDesk.Options;
using SimpleInjector;

namespace DependencyChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = InitContainer();
            var logger = container.GetInstance<ILogger>();
            var config = container.GetInstance<IConfigurationService<ConfigurationModel>>().Get();
            var service = container.GetInstance<IDependencyProvider>();

            logger.LogInformation("", $"*** Starting Dependency Resolver Version {config.Version} *** ");
            Console.WriteLine();

            var showHelp = false;
            var dirPath = "";
            var filePath = "";

            if (args.Any())
            {
                try
                {
                    var extras = ParseArgs(args, ref dirPath, ref filePath, ref showHelp);

                    //invalid switches found , show help and exit
                    if (extras.Any())
                    {
                        logger.LogInformation("args parser",
                            $"the following switches are not valid : {string.Join(",", extras)}");
                        ShowHelp();
                        return;
                    }
                }
                catch (OptionException e)
                {
                    logger.LogError("args parser", e.Message, e);
                }
                catch (Exception e)
                {
                    logger.LogError("args parser", "error in parsing argumants", e);
                }

                if (showHelp)
                {
                    ShowHelp();
                    return;
                }

                if (!string.IsNullOrEmpty(dirPath))
                    service.ValidateDirectory(dirPath);
                else
                    service.ValidateFile(filePath);
            }
            else
            {
                Console.WriteLine("Enter file address or directory path :");
                var path = Console.ReadLine();

                //check wether given path is file or not 
                if (string.IsNullOrEmpty(Path.GetFileName(path)) || Path.GetFileName(path)?.Split('.').Length < 2)
                {
                    if (Directory.Exists(path))
                        service.ValidateDirectory(path);
                    else
                    {
                        Console.WriteLine("directory not found");
                    }
                }
                else
                {
                    if (File.Exists(path))
                        service.ValidateFile(path);
                    else
                    {
                        Console.WriteLine("file not found");
                    }
                }
                Console.WriteLine("Press any key to exit");
                Console.Read();
            }
        }

        private static Container InitContainer()
        {
            var container =  new Register().Init();
            container.Register<IDependencyProvider, DependencyProvider>();
            container
                .RegisterSingleton<IConfigurationService<ConfigurationModel>,
                    JsonConfigurationService<ConfigurationModel>>();
            return container;
        }

        private static void ShowHelp()
        {
            Console.WriteLine(@"#-------------HELP-------------#
-d -dir -directory  Path to directory full of dependency files
-f -file            Path to a single dependecy file
-c                  Ignores Circular Dependency
-h -help            Show Help Page
#------------------------------#");

        }

        private static List<string> ParseArgs(string[] args,ref string dirPath, ref string filePath, ref bool showHelp)
        {
            string directoryPathTemp = "";
            string filePathTemp = "";
            bool showHelpTemp = false;
            var os = new OptionSet()
            {
                {
                    "d|dir|directory=", "the directory of dependency files", value =>
                    {

                        if (!Directory.Exists(value))
                            throw new Exception("directory not found");
                        directoryPathTemp = value;
                    }
                },
                {
                    "f|file=", "dependency file path", value =>
                    {
                        if (!File.Exists(value))
                            throw new Exception("file not found");
                        filePathTemp = value;
                    }
                },
                {
                    "c", "ignore circular dependecy ", value =>
                    {
                        DependecyNode.IgnoreCircularDependecy = true;
                    }
                },
                {
                    "h|help", "show help", value =>
                    {
                        showHelpTemp = true;
                    }
                }
            };
            var extras = os.Parse(args);
            dirPath = directoryPathTemp;
            filePath = filePathTemp;
            showHelp = showHelpTemp;
            return extras;
        }
    }
}
