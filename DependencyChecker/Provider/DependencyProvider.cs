﻿using System;
using System.IO;
using System.Linq;
using DC.Domain.Command;
using DC.Domain.Seedwork;
using DC.Utils.Interface;

namespace DependencyChecker.Provider
{
    public class DependencyProvider : IDependencyProvider
    {
        private readonly ICommandExecuter<ResolveDependeciesCommand, bool> _validateDependeciesCommandExecuter;
        private readonly ILogger _logger;

        public DependencyProvider(ICommandExecuter<ResolveDependeciesCommand, bool> validateDependeciesCommandExecuter,
            ILogger logger)
        {
            _validateDependeciesCommandExecuter = validateDependeciesCommandExecuter;
            _logger = logger;
        }

        public void ValidateDirectory(string dirPath)
        {
            var files = Directory.GetFiles(dirPath);
            foreach (var file in files)
            {
                ValidateFile(file);
            }
        }

        public void ValidateFile(string filePath)
        {
            try
            {
                var lines = File.ReadAllLines(filePath).Select(i => i.Trim()).ToArray();
                var validateCommand = new ResolveDependeciesCommand(lines);
                var result = _validateDependeciesCommandExecuter.Execute(validateCommand);
                var r = result ? "PASS" : "FAIL";
                var msg = Path.GetFileNameWithoutExtension(filePath) + ":" + r;
                _logger.LogInformation(GetType().Name, msg);

                //write an output file
                File.WriteAllText(
                    Path.Combine(Path.GetDirectoryName(filePath),
                        Path.GetFileName(filePath).Replace("input", "output")),
                    r);
            }
            catch (Exception e)
            {
                _logger.LogError("FileValidate", e.Message, e);
            }
        }
    }
}