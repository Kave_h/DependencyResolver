﻿namespace DependencyChecker.Provider
{
    public interface IDependencyProvider
    {
        void ValidateDirectory(string dirPath);
        void ValidateFile(string filePath);
    }
}