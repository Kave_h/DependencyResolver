﻿using System;
using System.IO;
using System.Linq;
using DC.Bootstrapper;
using DC.Domain;
using DC.Domain.Command;
using DC.Domain.Model;
using DC.Domain.Seedwork;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleInjector;

namespace DependencyResolver.Test
{
    [TestClass]
    public class ResolverTests
    {
        private Container _container;
        private ICommandExecuter<ResolveDependeciesCommand,bool> _resolveDependenciesCommandExecuter;
        private IDependencyResolverService _dependencyResolverService;

        [TestInitialize]
        public void Init()
        {
            _container = new Register().Init();
            _resolveDependenciesCommandExecuter =
                _container.GetInstance<ICommandExecuter<ResolveDependeciesCommand, bool>>();
            _dependencyResolverService = _container.GetInstance<IDependencyResolverService>();

        }

        [TestMethod]
        public void Should_Pass_When_TreeIsCreatedOk()
        {
            var text = @"1
            P1,42
            1
            P1,42,P2,Beta - 1";
            var tree = _dependencyResolverService.CreateDependecyTree(GetLines(text));
            Assert.AreEqual(tree.Package.Equals(Package.Empty()), true, "root node is invalid");
            Assert.AreEqual(tree.DependOnPackages.Count, 1, "root has invalid child number");
            tree = tree.DependOnPackages.First();
            Assert.AreEqual(tree.Package.Equals(Package.New("P1", "42")), true, "root node is invalid");
            Assert.AreEqual(tree.DependOnPackages.Count, 1, "node has invalid child number");
            Assert.AreEqual(tree.DependOnPackages.First().Package.Equals(Package.New("P2", "Beta - 1")), true,
                "child node is invalid");
        }

        [TestMethod]
        public void Should_Pass_When_TreeIsCreatedOk2()
        {
            var text = @"1
                A,1
                2
                A,1,B,2
                A,1,B,1
                ";
            var tree = _dependencyResolverService.CreateDependecyTree(GetLines(text));

            Assert.AreEqual(tree.Package.Equals(Package.Empty()), true, "root node is invalid");
            Assert.AreEqual(tree.DependOnPackages.Count, 1, "root has invalid child number");
            tree = tree.DependOnPackages.First();
            Assert.AreEqual(tree.Package.Equals(Package.New("A", "1")), true, "root node is invalid");
            Assert.AreEqual(tree.DependOnPackages.Count, 2, "node has invalid child number");
            Assert.AreEqual(tree.DependOnPackages.First().Package.Equals(Package.New("B", "2")), true,
                "first child node is invalid");
            Assert.AreEqual(tree.DependOnPackages.Last().Package.Equals(Package.New("B", "1")), true,
                "second child node is invalid");
        }

        [TestMethod]
        public void Should_Pass_When_TreeIsValid()
        {
            var root = CreateValidDependencyTree();
            var result = _dependencyResolverService.ValidateTree(root);
            Assert.AreEqual(result.isValid, true);
            Assert.AreEqual(result.validationErrorType, ResolveErrorType.None);
        }

        [TestMethod]
        public void Should_Fail_When_TreeHasDuplicateDependency()
        {
            var root = CreateDuplicateVersionDependencyTree();
            var result = _dependencyResolverService.ValidateTree(root);
            Assert.AreEqual(result.isValid, false);
            Assert.AreEqual(result.validationErrorType, ResolveErrorType.MultipleVersionsSamePackage);
        }

        [TestMethod]
        public void Should_Fail_When_TreeHasCircularDependency()
        {
            DependecyNode.IgnoreCircularDependecy = false;
            var root = CreateCircularDependencyDependencyTree();
            var result = _dependencyResolverService.ValidateTree(root);
            Assert.AreEqual(result.isValid, false);
            Assert.AreEqual(result.validationErrorType, ResolveErrorType.CircularDependency);
        }

        [TestMethod]
        public void Should_Pass_When_TreeHasCircularDependency()
        {
            DependecyNode.IgnoreCircularDependecy = true;
            var root = CreateCircularDependencyDependencyTree();
            var result = _dependencyResolverService.ValidateTree(root);
            Assert.AreEqual(result.isValid, true);
            Assert.AreEqual(result.validationErrorType, ResolveErrorType.None);
        }

        [TestMethod]
        public void Should_Pass_When_ScriptIsValid()
        {
            var text = @"1
                    P1,42
                    1
                    P1,42,P2,Beta-1";
            var command = new ResolveDependeciesCommand(GetLines(text));
            var result = _resolveDependenciesCommandExecuter.Execute(command);
            Assert.AreEqual(result,true);
        }

        [TestMethod]
        public void Should_Fail_When_ScriptHasDuplicate()
        {
            var text = @"1
                    A,1
                    2
                    A,1,B,2
                    A,1,B,1";
            var command = new ResolveDependeciesCommand(GetLines(text));
            var result = _resolveDependenciesCommandExecuter.Execute(command);
            Assert.AreEqual(result, false);
        }

        [TestMethod]
        public void Should_Fail_When_ScriptHasDuplicate2()
        {
            var text = @"1
                    B,2
                    2
                    B,2,A,1,C,1
                    C,1,A,2";
            var command = new ResolveDependeciesCommand(GetLines(text));
            var result = _resolveDependenciesCommandExecuter.Execute(command);
            Assert.AreEqual(result, false);
        }

        [TestMethod]
        public void Should_Fail_When_ScriptHasDuplicate3()
        {
            var text = @"2
                        A,1
                        C,1
                        2
                        A,1,B,1
                        C,1,B,2";
            var command = new ResolveDependeciesCommand(GetLines(text));
            var result = _resolveDependenciesCommandExecuter.Execute(command);
            Assert.AreEqual(result, false);
        }

        [TestMethod]
        public void Should_Pass_When_ScriptHasSelfDependecy()
        {
            var text = @"1
                        B,1
                        1
                        B,1,B,1";
            var command = new ResolveDependeciesCommand(GetLines(text));
            var result = _resolveDependenciesCommandExecuter.Execute(command);
            Assert.AreEqual(result, true);
        }

        [TestMethod]
        public void Should_Fail_When_ScriptHasCircularDependency()
        {
            var text = @"1
                        B,2
                        2
                        A,1,B,2
                        B,2,A,1
                        ";
            DependecyNode.IgnoreCircularDependecy = false;
            var command = new ResolveDependeciesCommand(GetLines(text));
            var result = _resolveDependenciesCommandExecuter.Execute(command);
            Assert.AreEqual(result, false);
        }

        [TestMethod]
        public void Should_Pass_When_ScriptHasCircularDependency()
        {
            var text = @"1
                        B,2
                        2
                        A,1,B,2
                        B,2,A,1
                        ";
            DependecyNode.IgnoreCircularDependecy = true;
            var command = new ResolveDependeciesCommand(GetLines(text));
            var result = _resolveDependenciesCommandExecuter.Execute(command);
            Assert.AreEqual(result, true);
        }

        #region private 
        private string[] GetLines(string str)
        {
            return str.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).Select(i => i.Trim())
                .ToArray();
        }

        private DependecyNode CreateValidDependencyTree()
        {
            var root = DependecyNode.CreateNode(Package.Empty());
            var node = root;
            var firstChild = DependecyNode.CreateNode(Package.New("A", "1"));
            node.AddChild(firstChild);
            node = firstChild;
            var secondChild = DependecyNode.CreateNode(Package.New("B", "1"));
            node.AddChild(secondChild);
            var thirdChild = DependecyNode.CreateNode(Package.New("C", "2"));
            node.AddChild(thirdChild);
            return root;
        }

        private DependecyNode CreateDuplicateVersionDependencyTree()
        {
            var root = DependecyNode.CreateNode(Package.Empty());
            var node = root;
            var firstChild = DependecyNode.CreateNode(Package.New("A", "1"));
            node.AddChild(firstChild);
            node = firstChild;
            var secondChild = DependecyNode.CreateNode(Package.New("B", "1"));
            node.AddChild(secondChild);
            var thirdChild = DependecyNode.CreateNode(Package.New("B", "2"));
            node.AddChild(thirdChild);
            return root;
        }

        private DependecyNode CreateCircularDependencyDependencyTree()
        {
            var root = DependecyNode.CreateNode(Package.Empty());
            var node = root;
            var firstChild = DependecyNode.CreateNode(Package.New("A", "1"));
            node.AddChild(firstChild);
            node = firstChild;
            var secondChild = DependecyNode.CreateNode(Package.New("B", "1"));
            node.AddChild(secondChild);
            node = secondChild;
            node.AddChild(firstChild);
            return root;
        }
        #endregion 
    }
}
