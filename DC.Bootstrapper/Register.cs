﻿using System;
using System.Linq;
using System.Runtime.Caching;
using AutoMapper;
using DC.Domain;
using DC.Domain.Seedwork;
using DC.Infrastructure;
using DC.Utils.AutoMapper;
using DC.Utils.Cache.MemoryCache;
using DC.Utils.DefaultLogger;
using DC.Utils.Interface;
using DC.Utils.NewtonSoftSerializer;
using SimpleInjector;
using IObjectMapper = DC.Utils.Interface.IObjectMapper;

namespace DC.Bootstrapper
{
    public class Register
    {
        public Container Init()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies().AsEnumerable();

            var container = new Container();

            container.RegisterSingleton<ISerializer, NewtonSoftSerializer>();
            
            container.RegisterSingleton<ICacher, MemoryCacheService>();
            container.RegisterSingleton<ILogger, DCLogger>();

            container.Register<IDependencyResolverService, DependencyResolverService>();

            container.Register(typeof(ICommandExecuter<,>), assemblies);
            container.Register(typeof(ICommandValidator<>), assemblies);

            container.RegisterDecorator(typeof(ICommandExecuter<,>), typeof(CommandValidatorDecorator<,>));

            container.RegisterSingleton(typeof(IMapper), () =>
            {
                var mapperConfig = new MapperConfiguration(MapConfiguration);
                return mapperConfig.CreateMapper();
            });
            container.RegisterSingleton<IObjectMapper, AutoMapperWrapper>();

            return container;
        }

        private void MapConfiguration(IMapperConfigurationExpression config)
        {
            config.CreateMap<CacheConfiguration, CacheItemPolicy>()
                .ForMember(dest => dest.SlidingExpiration,
                    opt => opt.MapFrom(src => src.SlidingExpiration != default(TimeSpan)
                        ? src.SlidingExpiration
                        : TimeSpan.Zero
                    ))
                .ForMember(dest => dest.AbsoluteExpiration,
                    opt => opt.MapFrom(src => src.AbsoluteExpiration != default(DateTimeOffset)
                        ? src.AbsoluteExpiration
                        : DateTimeOffset.MaxValue
                    ));
        }
    }
}
